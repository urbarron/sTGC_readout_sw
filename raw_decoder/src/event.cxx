#include "event.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


event::event() :
  EVID(-1),
  ok(false)
{
  udp_id= new std::vector<uint32_t>;
  bcid= new std::vector<uint32_t>;
  bcid_rel= new std::vector<uint32_t>;
  elink_id= new std::vector<uint32_t>;
  vmm_id= new std::vector<uint32_t>;
  chan_id= new std::vector<uint32_t>;
  tdo= new std::vector<uint32_t>;
  pdo= new std::vector<uint32_t>;

  clear();
}

void event::clear()
{
  EVID = -1;
  nVMM_hits = 0;
  udp_id->resize(0);
  bcid->resize(0);
  bcid_rel->resize(0);
  elink_id->resize(0);
  vmm_id->resize(0);
  chan_id->resize(0);
  tdo->resize(0);
  pdo->resize(0);
}

hit event::getHit( int ihit ) 
{

  hit m_hit;

  if ( ihit >= udp_id->size() ) {
    std::cout << "Error: trying to access a hit from event that doesn't exist" << std::endl;
    return m_hit;
  }

  m_hit.udp_id   = udp_id->at(ihit);
  m_hit.bcid     = bcid->at(ihit);
  m_hit.bcid_rel = bcid_rel->at(ihit);
  m_hit.elink_id = elink_id->at(ihit);
  m_hit.vmm_id   = vmm_id->at(ihit);
  m_hit.chan_id  = chan_id->at(ihit);
  m_hit.tdo      = tdo->at(ihit);
  m_hit.pdo      = pdo->at(ihit);

  return m_hit;

}

void event::addHit( hit m_hit ) {
  
  nVMM_hits++;

  udp_id->  push_back( m_hit.udp_id );
  bcid->    push_back( m_hit.bcid );
  bcid_rel->push_back( m_hit.bcid_rel );
  elink_id->push_back( m_hit.elink_id );
  vmm_id->  push_back( m_hit.vmm_id );
  chan_id-> push_back( m_hit.chan_id );
  tdo->     push_back( m_hit.tdo );
  pdo->     push_back( m_hit.pdo );

}
