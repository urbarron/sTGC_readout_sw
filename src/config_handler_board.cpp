// vmm
#include "config_handler_board.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>



//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandlerBoard -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerBoard::ConfigHandlerBoard(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    //    m_board_selection(-1),
    m_msg(0)
{
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerBoard::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
//// ------------------------------------------------------------------------ //
bool ConfigHandlerBoard::LoadBoardConfig_from_File(const QString &filename)
{

  m_commSettings.config_filename = filename;

  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree pt;
  read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);

  m_board = LoadBoardSettings(pt);
  bool board_ok = m_board.ok;
  if(!board_ok) {
    msg()("Problem loading GlobalSettings", "ConfigHandlerBoard::LoadConfig");
  }

  if(!( board_ok ) ) { 
    msg()("Configuration loaded unsucessfully","ConfigHandlerBoard::LoadConfig");
    if(!board_ok)      cout << "board_ok" << endl;
  }

  return (board_ok); 
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerBoard::WriteBoardConfig_to_File(QString filename)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;

  ptree out_root;

  ptree out_board;
  out_board.put("boardType",   BoardSettings().boardType);
  out_board.put("boardID",     BoardSettings().boardID);
  out_board.put("boardUN",     BoardSettings().boardUN.toStdString());
  out_board.put("nVMM",        BoardSettings().nVMM);
  out_board.put("nTDS",        BoardSettings().nTDS);
  out_board.put("ROC_UniqueS", BoardSettings().ROC_UniqueS.toStdString());
  
  //  std::cout << BoardSettings().ROC_UniqueS.toStdString() << std::endl;

  stringstream chnode;
  for ( int i=0; i<BoardSettings().nVMM; i++ ) {
    ptree out_vmm;

    ss.str("");
    ss << "VMM_" << format("%01i") % i;

    chnode.str("");
    chnode << "VMM_UniqueS";
    out_vmm.put(chnode.str(), BoardSettings().VMM_UniqueS_Numbers.at(i).toStdString());

    chnode.str("");
    chnode << "VMMChip_ID";
    out_vmm.put(chnode.str(), BoardSettings().VMMChip_ID.at(i));
    chnode.str("");

    out_board.add_child(ss.str(), out_vmm);
  }

  for ( int i=0; i<BoardSettings().nTDS; i++ ) {
    ptree out_tds;

    ss.str("");
    ss << "TDS_" << format("%01i") % i;

    chnode.str("");
    chnode << "TDS_UniqueS";
    out_tds.put(chnode.str(), BoardSettings().TDS_UniqueS_Numbers.at(i).toStdString());

    chnode.str("");
    chnode << "TDSChip_ID";
    out_tds.put(chnode.str(), BoardSettings().TDSChip_ID.at(i));
    chnode.str("");

    out_board.add_child(ss.str(), out_tds);
  }


  for ( int i=0; i<BoardSettings().GPIO_dir_enable.size(); i++ ) {
    ptree out_GPIO;

    ss.str("");
    ss << "GPIO_" << format("%02i") % i;

    chnode.str("");
    chnode << "GPIO_dir_chan_enable";
    out_GPIO.put(chnode.str(), BoardSettings().GPIO_dir_enable.at(i));

    chnode.str("");
    chnode << "GPIO_dout_chan_enable";
    out_GPIO.put(chnode.str(), BoardSettings().GPIO_dout_enable.at(i));
    chnode.str("");

    out_board.add_child(ss.str(), out_GPIO);

  }

  out_root.add_child("board_settings", out_board);
  outpt.add_child("configuration", out_root);

  stringstream sx;
  try {
        #if BOOST_VERSION >= 105800
    write_xml(filename.toStdString(), outpt, std::locale(),
              boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
    write_xml(filename.toStdString(), outpt, std::locale(),
              boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

    sx.str("");
    sx << "Configuration written successfully to file: \n";
    sx << " > " << filename.toStdString();
    msg()(sx,"ConfigHandlerBoard::WriteConfig");
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Unable to write output configuration XML file: " << e.what();
    msg()(sx,"ConfigHandlerBoard::WriteConfig");
    return;
  }


}
//// ------------------------------------------------------------------------ //
CommInfo ConfigHandlerBoard::LoadCommInfo(const boost::property_tree::ptree& pt)
{
    using boost::property_tree::ptree;

    CommInfo comm;

    try
    {
        for(const auto& conf : pt.get_child("configuration")) {
            if(!(conf.first == "udp_setup"))
                continue;

            if(conf.first=="udp_setup") {
                // FEC port
	        //   comm.fec_port = conf.second.get<int>("fec_port");
		// stgc port
		comm.host_port = conf.second.get<int>("host_port");
                // DAQ port
                comm.target_port = conf.second.get<int>("target_port");
                // VMMASIC port
		//   comm.vmmasic_port = conf.second.get<int>("vmmasic_port");
                // VMMAPP port
		//   comm.vmmapp_port = conf.second.get<int>("vmmapp_port");
                // clocks port
                //   comm.clocks_port = conf.second.get<int>("clocks_port");

                comm.ok = true;
            } // udp_setup
        }
    }
    catch(std::exception &e)
    {
        stringstream sx;
        sx << "!! -------------------------------------------- !! \n"
           << "  ERROR CONF: " << e.what() << "\n"
           << "!! -------------------------------------------- !!"; 
        msg()(sx,"ConfigHandlerBoard::LoadCommInfo"); 
        comm.ok = false;
    }

    return comm;
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerBoard::LoadCommInfo(const CommInfo& info)
{
    m_commSettings = info;
    if(dbg())
        m_commSettings.print();
}
// -------------------------------------------------------------------------- //
BoardSetting ConfigHandlerBoard::LoadBoardSettings( const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;
  using boost::format;

  stringstream sx;

  BoardSetting g;

  bool outok = true;

  try{
    for(const auto& conf : pt.get_child("configuration")) {
      if(!(conf.first == "board_settings")) continue;

      g.boardType       = conf.second.get<int>("boardType");
      g.boardID         = conf.second.get<int>("boardID");
      g.boardUN         = QString::fromStdString( conf.second.get<std::string>("boardUN") );
      g.nVMM            = conf.second.get<int>("nVMM");
      g.nTDS            = conf.second.get<int>("nTDS");
      g.ROC_UniqueS     = QString::fromStdString( conf.second.get<std::string>("ROC_UniqueS") );
    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading board settings: " << e.what();
    msg()(sx,"ConfigHandlerBoard::LoadBoardSettings");
    outok = false;
  } 

  stringstream ss,where;

  try {
    for(int i = 0; i < g.nVMM; i++) {
      ss.str("");
      ss << "VMM_" << format("%01i") % i;
      for(const auto& conf : pt.get_child("configuration.board_settings")) {
        size_t find = conf.first.find(ss.str());
        if(find==string::npos) continue;
	//        where.str("");
	//        where << "(ch. " << format("%01i") % i;
	//        string value = "";

	std::string trim;
        trim = conf.second.get<std::string>("VMM_UniqueS");
        g.VMM_UniqueS_Numbers.push_back(QString::fromStdString(trim));

        int val;
        val = conf.second.get<int>("VMMChip_ID");
        g.VMMChip_ID.push_back(val);

      }
    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading board settings: " << e.what();
    msg()(sx,"ConfigHandlerBoard::LoadBoardSettings");
    outok = false;
  } 


  try {
    for(int i = 0; i < g.nTDS; i++) {
      ss.str("");
      ss << "TDS_" << format("%01i") % i;
      for(const auto& conf : pt.get_child("configuration.board_settings")) {
        size_t find = conf.first.find(ss.str());
        if(find==string::npos) continue;
        where.str("");
        where << "(ch. " << format("%01i") % i;
        string value = "";

	std::string trim;
        trim = conf.second.get<std::string>("TDS_UniqueS");
        g.TDS_UniqueS_Numbers.push_back(QString::fromStdString(trim));

        int val;
        val = conf.second.get<int>("TDSChip_ID");
        g.TDSChip_ID.push_back(val);

      }
    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading board TDS settings: " << e.what();
    msg()(sx,"ConfigHandlerBoard::LoadBoardSettings");
    outok = false;
  }

  try {
    //    g.GPIO_dir_enable.resize(0);
    //    g.GPIO_dout_enable.resize(0);

    for(int i = 0; i < 32; i++) {
      ss.str("");
      ss << "GPIO_" << format("%02i") % i;
      for(const auto& conf : pt.get_child("configuration.board_settings")) {
        size_t find = conf.first.find(ss.str());
        if(find==string::npos) continue;

	bool val;
        val = conf.second.get<bool>("GPIO_dir_chan_enable");
        g.GPIO_dir_enable.push_back(val);

        val = conf.second.get<bool>("GPIO_dout_chan_enable");
        g.GPIO_dout_enable.push_back(val);
      }
    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading board GPIO settings: " << e.what();
    msg()(sx,"ConfigHandlerBoard::LoadBoardSettings");
    outok = false;
  }


    
  g.ok = outok;

  return g;

}
//// ------------------------------------------------------------------------ //
void ConfigHandlerBoard::LoadBoardConfiguration(BoardSetting& board)
{
  m_board = board;
}
