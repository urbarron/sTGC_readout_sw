#include "daq_buffer.h"

#include <iostream>
using std::cout;
using std::endl;

// moodycamel
using namespace moodycamel;


///////////////////////////////////////////////////////////////
// DaqBuffer
///////////////////////////////////////////////////////////////
DaqBuffer::DaqBuffer() :
    m_queue(NULL),
    m_capacity(15)
{
}
///////////////////////////////////////////////////////////////
void DaqBuffer::initialize(size_t capacity)
{
    cout << "DaqBuffer::initialize    Initializing queue with capacity: " << capacity << endl; 

    m_capacity = capacity;
    m_queue = boost::shared_ptr<ReaderWriterQueue<IPDataArray> >
        (new ReaderWriterQueue<IPDataArray>(m_capacity));
}
///////////////////////////////////////////////////////////////
bool DaqBuffer::has_data()
{
    if(m_queue) {
        return m_queue->peek();
    }
    else {
        cout << "DaqBuffer::has_data    WARNING Buffer not initialized. Returning false. " << endl;
        return false;
    }
}
///////////////////////////////////////////////////////////////
bool DaqBuffer::dequeue(IPDataArray& element)
{
    return m_queue->try_dequeue(element);
    //return m_queue->wait_dequeue_timed(element, 100); // us
    //return m_queue->wait_dequeue_timed(element, std::chrono::milliseconds(1));
    //if(m_queue) {
    //    return m_queue->wait_dequeue_timed(element, std::chrono::milliseconds(1));
    //}
    //else {
    //    cout << "DaqBuffer::wait_for_dequeue    WARNING Buffer not initialized. Returning false." << endl;
    //    return false;
    //}
}
///////////////////////////////////////////////////////////////
bool DaqBuffer::add_to_queue(IPDataArray& element)
{
    if(m_queue) {
        return m_queue->enqueue(element);
    }
    else {
        cout << "DaqBuffer::add_to_queue    WARNING Buffer not initialized. Returning false." << endl;
        return false;
    }
}




