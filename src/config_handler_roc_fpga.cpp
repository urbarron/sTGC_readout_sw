// vmm
#include "config_handler_roc_fpga.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>



//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandlerTDS -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerRocFPGA::ConfigHandlerRocFPGA(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    m_msg(0)
{
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocFPGA::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocFPGA::SetBoardID( int boardID ){
  m_roc_fpga_settings.boardID = boardID;
}
void ConfigHandlerRocFPGA::SetBoardType( int boardType ){
  m_roc_fpga_settings.board_Type = boardType;
}

bool ConfigHandlerRocFPGA::Load_ROC_FPGAConfig_from_File(const QString &filename)
{

  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree pt;
  read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);

  // Load the global configuration
  m_roc_fpga_settings = Load_ROC_FPGASettings(pt);
  bool global_ok = m_roc_fpga_settings.ok;
  if(!global_ok) {
    msg()("Problem loading ROC FPGA Settings", "ConfigHandlerRocFPGA::Load_Roc_FPGAConfig_from_File");
  }

  return ( global_ok ); //daq_ok && m_vmmMap.ok && vmmchan_ok && clocks_ok);
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocFPGA::Write_ROC_FPGAConfig_to_File(QString filename)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;   

  ptree out_root;

  // ----------------------------------------------- //
  //  global settings
  // ----------------------------------------------- //

  ptree out_global;
  out_global.put("header",               ROC_FPGASettings().header);
  out_global.put("board_Type",           ROC_FPGASettings().board_Type);
  out_global.put("boardID",              ROC_FPGASettings().boardID);
  out_global.put("ASIC_Type",            ROC_FPGASettings().ASIC_Type);
  out_global.put("chipID",               ROC_FPGASettings().chipID);
  out_global.put("command",              ROC_FPGASettings().command);
  out_global.put("UniqueS",              ROC_FPGASettings().UniqueS);
  out_global.put("vmm_enable_mask",      ROC_FPGASettings().vmm_enable_mask);
  out_global.put("cktp_vs_ckbc_skew",    ROC_FPGASettings().cktp_vs_ckbc_skew);
  out_global.put("cktp_width",           ROC_FPGASettings().cktp_width);
  out_global.put("trigger_window_width", ROC_FPGASettings().trigger_window_width);

  // ----------------------------------------------- //
  // stitch together the fields
  // ----------------------------------------------- //
  out_root.add_child("global_registers", out_global);

  // put everything under a global node
  outpt.add_child("configuration", out_root);


  stringstream sx;
  try {
        #if BOOST_VERSION >= 105800
    write_xml(filename.toStdString(), outpt, std::locale(),
	      boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
    write_xml(filename.toStdString(), outpt, std::locale(),
	      boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

    sx.str("");
    sx << "Configuration written successfully to file: \n";
    sx << " > " << filename.toStdString();
    msg()(sx,"ConfigHandlerRocFPGA::WriteConfig");
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Unable to write output configuration XML file: " << e.what();
    msg()(sx,"ConfigHandlerRocFPGA::WriteConfig");
    return;
  }

  return;

}
//// ------------------------------------------------------------------------ //
ROC_FPGASetting ConfigHandlerRocFPGA::Load_ROC_FPGASettings(const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;

  stringstream sx;

  ROC_FPGASetting g;

  bool outok = true;

  try{
    for(const auto& conf : pt.get_child("configuration")) {
      if(!(conf.first == "global_registers")) continue;

      g.header          = conf.second.get<string>("header");
      g.board_Type      = conf.second.get<int>("board_Type");
      g.boardID         = conf.second.get<int>("boardID");
      g.ASIC_Type       = conf.second.get<int>("ASIC_Type");
      g.chipID          = conf.second.get<int>("chipID");
      g.command         = conf.second.get<string>("command");
      g.UniqueS         = conf.second.get<string>("UniqueS");

      g.vmm_enable_mask      = conf.second.get<uint32_t>("vmm_enable_mask");
      g.cktp_vs_ckbc_skew    = conf.second.get<uint32_t>("cktp_vs_ckbc_skew");
      g.cktp_width           = conf.second.get<uint32_t>("cktp_width");
      g.trigger_window_width = conf.second.get<uint32_t>("trigger_window_width");

     }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading ROC FPGA registers: " << e.what();
    msg()(sx,"ConfigHandlerRocFPGA::Load_Roc_FPGASettings");
    outok = false;
  } // catch                                                                                       

  g.ok = outok;

  m_roc_fpga_settings = g;

  return g;

}
//// ------------------------------------------------------------------------ //                     
QString ConfigHandlerRocFPGA::ROC_FPGAMask(){

  QString MapString = "0000000000000000";
  QString str;
  int pos = 0;

  str = QString("%1").arg(m_roc_fpga_settings.board_Type, 2, 2, QChar('0'));
  MapString.replace(1, str.size(), str);
  pos+=1+str.size();

  str = QString("%1").arg(m_roc_fpga_settings.boardID, 3, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=pos+str.size();

  str = QString("%1").arg(m_roc_fpga_settings.ASIC_Type, 2, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(m_roc_fpga_settings.chipID, 8, 2, QChar('0'));

  /*
  if ( m_roc_fpga_settings.chipID == 1 ) str = "00000001";
  if ( m_roc_fpga_settings.chipID == 2 ) str = "00000010";
  if ( m_roc_fpga_settings.chipID == 3 ) str = "00000011";
  if ( m_roc_fpga_settings.chipID == 4 ) str = "00000100";
  if ( m_roc_fpga_settings.chipID == 5 ) str = "00000101";
  if ( m_roc_fpga_settings.chipID == 6 ) str = "00000110";
  if ( m_roc_fpga_settings.chipID == 7 ) str = "00000111";
  if ( m_roc_fpga_settings.chipID == 8 ) str = "00001000";
  */

  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  return MapString;

}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocFPGA::Load_ROC_FPGAConfiguration(ROC_FPGASetting & global)
{
  m_roc_fpga_settings = global;
  return;
}
// -------------------------------------------------------------------------- //
void ConfigHandlerRocFPGA::Set_ROC_Registered_to_Default(int boardID, int boardType)
{
  // default values that turns all GPIOs on for the board on elink number boardID

  m_roc_fpga_settings.header               = "decafbad";
  m_roc_fpga_settings.boardID              = boardID;
  m_roc_fpga_settings.board_Type           = boardType;
  m_roc_fpga_settings.ASIC_Type            = 2; //ROC
  m_roc_fpga_settings.chipID               = 0;
  m_roc_fpga_settings.command              = "0200"; //configure ASIC
  m_roc_fpga_settings.UniqueS              = "";
  m_roc_fpga_settings.vmm_enable_mask      = 255; //8hFF all VMM are enabled
  m_roc_fpga_settings.cktp_vs_ckbc_skew    = 0;
  m_roc_fpga_settings.cktp_width           = 1023;
  m_roc_fpga_settings.trigger_window_width = 4800;

  return;

}
