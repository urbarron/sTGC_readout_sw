
// vmm
#include "config_module_roc_asic.h"
#include "vmm_decoder.h"
// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// Qt
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QBitArray>
#include <QStringList>

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_Roc_ASIC
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
Config_RocASIC::Config_RocASIC(Config_Base *parent) :
  Config_Base(parent),
  m_configHandler(0)
{
}
// ------------------------------------------------------------------------ //
Config_RocASIC& Config_RocASIC::LoadConfig(ConfigHandlerRocASIC& config)
{
    m_configHandler = &config;
    if(!m_configHandler) {
        msg()("FATAL ConfigHandler ASIC instance is null", "Config_RocASIC::LoadConfig", true);
        exit(1);
    }
    else if(dbg()) {
        msg()("ConfigHandler ASIC instance loaded", "Config_RocASIC::LoadConfig");
    }
    return *this;
}
//---------------------------------------------------// 
bool Config_RocASIC::SendRocASIC_DigitalConfig(int send_to_port, const QHostAddress &target_ip)
{
  stringstream sx;
  bool send_ok = true;
  bool ok;

  sx.str("");

  //////////////////////////////////////////////////
  // build the configuration word(s) to be send to  
  // the front ends                                 
  //////////////////////////////////////////////////

  QByteArray datagram;
  QDataStream out(&datagram, QIODevice::WriteOnly);
  out.device()->seek(0);

  //////////////////////////////////////////////////
  // Global Registers                               
  //////////////////////////////////////////////////
  std::vector<QString> RocASIC_GlobalRegisters;
  RocASIC_GlobalRegisters.clear();
  fillRocASIC_DigitalRegisters(RocASIC_GlobalRegisters);

  //----------------------------------------------//

  out << (quint32) QString::fromStdString(config().ROC_ASIC_Settings().header).toUInt(&ok,16);
  out << (quint16) Build_Mask(config().ROC_ASIC_Settings().board_Type, config().ROC_ASIC_Settings().boardID,
			      config().ROC_ASIC_Settings().ASIC_Type,  0).toUInt(&ok,16); // digital has chipID = 0
  out << (quint16) QString::fromStdString(config().ROC_ASIC_Settings().command).toUInt(&ok,16);

  //  sx.str("");
  //  sx << config().RocASICMap().toStdString() << "\n";
  //  msg()(sx,"Config_RocASIC::SendConfig");

  //------------------------------------------------------//
  //    128 bit register for ROC ASIC
  //------------------------------------------------------//

  // 32 bits of vmm_enable_mask, cktp_vs_ckbc_skew, cktp width (MSB first)
  out << (quint32)(RocASIC_GlobalRegisters.at(0).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(1).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(2).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(3).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(4).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(5).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(6).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(7).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(8).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(9).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(10).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(11).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(12).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(13).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(14).toUInt(&ok,2));
  out << (quint32)(RocASIC_GlobalRegisters.at(15).toUInt(&ok,2));

  //--------------------------------------------------------------//

  QString tmp = QString::fromStdString( datagram.toHex().toStdString() );
  send_ok = SendConfigN(send_to_port, target_ip, tmp, 1 );

  //boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

  return send_ok;

  //--------------------------------------------------------------//

}
// ------------------------------------------------------------------------ //
void Config_RocASIC::fillRocASIC_DigitalRegisters(std::vector<QString> &global)
{
  stringstream sx;
  if(dbg()) msg()("Loading global registers","Config_RocASIC::fillGlobalRegisters");
  global.resize(0);
  global.clear();
  int pos = 0;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  //-----------------------------------//
  //          first 32 bits
  //-----------------------------------//

  QString spi1_0 = bit32_empty;
  pos = 0;

  QString str;

  //------------------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_ASIC_Settings().ROC_ID, 6, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().even_parity, 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().L1_first, 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //-----------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_ASIC_Settings().elink_speed_sROC0, 2, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().elink_speed_sROC1, 2, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().elink_speed_sROC2, 2, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().elink_speed_sROC3, 2, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //----------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_ASIC_Settings().vmm_mask_sROC0, 8, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().vmm_mask_sROC1, 8, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();


  //-----------------------------------//  
  //          2nd 32 bits               
  //-----------------------------------//  

  QString spi1_1 = bit32_empty;
  pos = 0;

  str = QString("%1").arg(config().ROC_ASIC_Settings().vmm_mask_sROC2, 8, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().vmm_mask_sROC3, 8, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_ASIC_Settings().NullEvt_enable_sROC_mask, 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().EOP_enable_sROC_mask, 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_ASIC_Settings().enable_sROC_mask, 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().TTC_start_bits, 2, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().timeout_ena, 1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().bypass, 1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//
  //                        3rd 32 bits
  //---------------------------------------------------------------//

  QString spi1_2 = bit32_empty;
  pos = 0;

  str = QString("%1").arg(config().ROC_ASIC_Settings().vmm_enable_mask, 8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().timeout, 8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( ((config().ROC_ASIC_Settings().BC_offset & 0b111100000000) >> 8 ) , 4, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_ASIC_Settings().TX_current, 4, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //-------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_ASIC_Settings().BC_offset, 8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //-------------------------------------------------------------//
  //                       4th 32 bits
  //-------------------------------------------------------------//

  QString spi1_3 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( ((config().ROC_ASIC_Settings().BC_rollover & 0b111100000000) >> 8 ), 4, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  pos += 4; // skip the last 4 bits in first 8 bit word

  //------------------------------------------------------------//

  str = QString("%1").arg( config().ROC_ASIC_Settings().BC_rollover, 8, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //------------------------------------------------------------//

  str = QString("%1").arg( config().ROC_ASIC_Settings().eport_sROC0, 2, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( config().ROC_ASIC_Settings().eport_sROC1, 2, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( config().ROC_ASIC_Settings().eport_sROC2, 2, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( config().ROC_ASIC_Settings().eport_sROC3, 2, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //-----------------------------------------------------------//

  //   last 8 bits are padded 0's

  //-----------------------------------------------------------//
  //                      5th 32 bits
  //-----------------------------------------------------------//

  QString spi1_4 = bit32_empty;
  pos = 0;

  pos += 24; //  24 bits of 0's

  str = QString("%1").arg( config().ROC_ASIC_Settings().fake_vmm_failure_mask, 8, 2, QChar('0'));
  spi1_4.replace(pos, str.size(), str);
  pos+=str.size();

  //----------------------------------------------------------//
  //                     6th 32 bits
  //----------------------------------------------------------//

  QString spi1_5 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( config().ROC_ASIC_Settings().BUSY_enable_sROC_mask, 4, 2, QChar('0'));
  spi1_5.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( config().ROC_ASIC_Settings().TDS_enable_sROC_mask, 4, 2, QChar('0'));
  spi1_5.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------//

  str = QString("%1").arg( ((config().ROC_ASIC_Settings().BUSY_On_Limit & 0b111100000000 ) >> 8 ), 4, 2, QChar('0'));
  spi1_5.replace(pos, str.size(), str);
  pos+=str.size();

  pos+=4; // skip next 4 bits

  //--------------------------------------------------------//

  str = QString("%1").arg( config().ROC_ASIC_Settings().BUSY_On_Limit, 8, 2, QChar('0'));
  spi1_5.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------//

  str = QString("%1").arg( ((config().ROC_ASIC_Settings().BUSY_Off_Limit & 0b111100000000 ) >> 8 ), 4, 2, QChar('0'));
  spi1_5.replace(pos, str.size(), str);
  pos+=str.size();

  pos+=4; // skip next 4 bits

  //--------------------------------------------------------//
  //                      7th 32 bits
  //--------------------------------------------------------//

  QString spi1_6 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( config().ROC_ASIC_Settings().BUSY_Off_Limit, 8, 2, QChar('0'));
  spi1_6.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------//

  //  last 24 bits empty

  //--------------------------------------------------------//
  //                      8th 32 bits
  //--------------------------------------------------------//

  QString spi1_7 = bit32_empty;
  pos = 0;

  pos += 24; // first 24 bits empty

  str = QString("%1").arg( config().ROC_ASIC_Settings().Max_L1_Events_no_comma, 8, 2, QChar('0'));
  spi1_7.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------//
  //                   9-15th 32 bits
  //--------------------------------------------------------//

  QString spi1_8  = bit32_empty;
  QString spi1_9  = bit32_empty;
  QString spi1_10 = bit32_empty;
  QString spi1_11 = bit32_empty;
  QString spi1_12 = bit32_empty;
  QString spi1_13 = bit32_empty;
  QString spi1_14 = bit32_empty;
  QString spi1_15 = bit32_empty;

  //-------------------------------------------------------//

  global.push_back(spi1_0);
  global.push_back(spi1_1);
  global.push_back(spi1_2);
  global.push_back(spi1_3);
  global.push_back(spi1_4);
  global.push_back(spi1_5);
  global.push_back(spi1_6);
  global.push_back(spi1_7);
  global.push_back(spi1_8);
  global.push_back(spi1_9);
  global.push_back(spi1_10);
  global.push_back(spi1_11);
  global.push_back(spi1_12);
  global.push_back(spi1_13);
  global.push_back(spi1_14);
  global.push_back(spi1_15);

}

//---------------------------------------------------//
bool Config_RocASIC::SendRocASIC_AnalogConfig(int send_to_port, const QHostAddress &target_ip)
{
  stringstream sx;
  bool send_ok = true;
  bool ok;

  sx.str("");

  //////////////////////////////////////////////////   
  // build the configuration word(s) to be send to     
  // the front ends                                    
  //////////////////////////////////////////////////   

  QByteArray datagram;
  QDataStream out(&datagram, QIODevice::WriteOnly);
  out.device()->seek(0);

  //////////////////////////////////////////////////   
  // Global Registers                                  
  //////////////////////////////////////////////////   
  std::vector<QString> RocASIC_VMM0_Ana_ePllReg;
  RocASIC_VMM0_Ana_ePllReg.clear();
  fillRocASIC_Analog_epll(RocASIC_VMM0_Ana_ePllReg, VMM0_ePll);

  std::vector<QString> RocASIC_VMM1_Ana_ePllReg;
  RocASIC_VMM1_Ana_ePllReg.clear();
  fillRocASIC_Analog_epll(RocASIC_VMM1_Ana_ePllReg, VMM1_ePll);

  std::vector<QString> RocASIC_TDS_Ana_ePllReg;
  RocASIC_TDS_Ana_ePllReg.clear();
  fillRocASIC_Analog_epll(RocASIC_TDS_Ana_ePllReg,  TDS_ePll);

  std::vector<QString> RocASIC_Int_Ana_ePllReg;
  RocASIC_Int_Ana_ePllReg.clear();
  fillRocASIC_Analog_Int_epll(RocASIC_Int_Ana_ePllReg);

  //----------------------------------------------//    

  out << (quint32) QString::fromStdString(config().ROC_ASIC_Settings().header).toUInt(&ok,16);
  out << (quint16) Build_Mask(config().ROC_ASIC_Settings().board_Type, config().ROC_ASIC_Settings().boardID,
                              config().ROC_ASIC_Settings().ASIC_Type,  1).toUInt(&ok,16); // analog has chipID = 1;
  out << (quint16) QString::fromStdString(config().ROC_ASIC_Settings().command).toUInt(&ok,16);

  //  sx.str("");                                       
  //  sx << config().RocASICMap().toStdString() << "\n";
  //  msg()(sx,"Config_RocASIC::SendConfig");           

  //------------------------------------------------------//
  //    128 bit register for ROC ASIC                       
  //------------------------------------------------------//

  // 32 bits of vmm_enable_mask, cktp_vs_ckbc_skew, cktp width (MSB first)
  out << (quint32)(RocASIC_VMM0_Ana_ePllReg.at(0).toUInt(&ok,2));
  out << (quint32)(RocASIC_VMM0_Ana_ePllReg.at(1).toUInt(&ok,2));
  out << (quint32)(RocASIC_VMM0_Ana_ePllReg.at(2).toUInt(&ok,2));
  out << (quint32)(RocASIC_VMM0_Ana_ePllReg.at(3).toUInt(&ok,2));

  out << (quint32)(RocASIC_VMM1_Ana_ePllReg.at(0).toUInt(&ok,2));
  out << (quint32)(RocASIC_VMM1_Ana_ePllReg.at(1).toUInt(&ok,2));
  out << (quint32)(RocASIC_VMM1_Ana_ePllReg.at(2).toUInt(&ok,2));
  out << (quint32)(RocASIC_VMM1_Ana_ePllReg.at(3).toUInt(&ok,2));

  out << (quint32)(RocASIC_TDS_Ana_ePllReg.at(0).toUInt(&ok,2));
  out << (quint32)(RocASIC_TDS_Ana_ePllReg.at(1).toUInt(&ok,2));
  out << (quint32)(RocASIC_TDS_Ana_ePllReg.at(2).toUInt(&ok,2));
  out << (quint32)(RocASIC_TDS_Ana_ePllReg.at(3).toUInt(&ok,2));

  out << (quint32)(RocASIC_Int_Ana_ePllReg.at(0).toUInt(&ok,2));
  out << (quint32)(RocASIC_Int_Ana_ePllReg.at(1).toUInt(&ok,2));
  out << (quint32)(RocASIC_Int_Ana_ePllReg.at(2).toUInt(&ok,2));
  out << (quint32)(RocASIC_Int_Ana_ePllReg.at(3).toUInt(&ok,2));

  //--------------------------------------------------------------// 
  QString tmp = QString::fromStdString( datagram.toHex().toStdString() );
  send_ok = SendConfigN(send_to_port, target_ip, tmp, 1 );

  //boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
  return send_ok;

  //--------------------------------------------------------------// 
}
// ------------------------------------------------------------------------ //
void Config_RocASIC::fillRocASIC_Analog_epll(std::vector<QString> &epll_reg, int i_ePll)
{
  stringstream sx;
  if(dbg()) msg()("Loading global registers","Config_RocASIC::fillGlobalRegisters");
  int pos = 0;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  ROC_ASIC_analog_epll Analog_ePll_obj;
  
  if      ( i_ePll == VMM0_ePll ) Analog_ePll_obj = config().ROC_ASIC_Ana_VMM0_Settings();
  else if ( i_ePll == VMM1_ePll ) Analog_ePll_obj = config().ROC_ASIC_Ana_VMM1_Settings();
  else if ( i_ePll == TDS_ePll  ) Analog_ePll_obj = config().ROC_ASIC_Ana_TDS_Settings();
  else {
    msg()("i_ePll not recognized in filling Analog ROC Registers","Config_RocASIC::fillAnalog_ePllRegisters");
    return;
  }

  //-----------------------------------//
  //          first 32 bits              
  //-----------------------------------//

  QString spi1_0 = bit32_empty;
  pos = 0;

  QString str;

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_0, 7, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_0 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_1, 7, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_1 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_2, 7, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_2 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_3, 7, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_3 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//
  //                        2nd 32 bits                              
  //---------------------------------------------------------------//

  QString spi1_1 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_0 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_1 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_2 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_3 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePll_Ref_Freq,        2, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePll_cap,   2, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePllLockEn,      1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.bypassPll,       1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePllReset,       1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePllInstantLock, 1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePll_lcp, 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePll_res, 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//
  //                        3rd 32 bits                              
  //---------------------------------------------------------------//
  QString spi1_2 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( Analog_ePll_obj.ePllEnablePhase, 8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.tp_phase_0, 3, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.tp_bypass_mask & 0b0001), 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.tp_phase_1, 3, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.tp_bypass_mask & 0b0010) >> 1), 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.tp_phase_2, 3, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.tp_bypass_mask & 0b0100) >> 2), 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.tp_phase_3, 3, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.tp_bypass_mask & 0b1000) >> 3), 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //----------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ctrl_phase_0, 3, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.ctrl_bypass_mask & 0b1), 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ctrl_delay_0, 3, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.ctrl_delay_mask & 0b1), 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//
  //                        4th 32 bits                              
  //---------------------------------------------------------------//
  QString spi1_3 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( Analog_ePll_obj.ctrl_phase_1, 3, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ctrl_bypass_mask & 0b10) >> 1), 1, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ctrl_delay_1, 3, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ctrl_delay_mask & 0b10) >> 1), 1, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ctrl_phase_2, 3, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ctrl_bypass_mask & 0b100) >> 2), 1, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ctrl_delay_2, 3, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ctrl_delay_mask & 0b100) >> 2), 1, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ctrl_phase_3, 3, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ctrl_bypass_mask & 0b1000) >> 3), 1, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ctrl_delay_3, 3, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ctrl_delay_mask & 0b10) >> 3), 1, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //-------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.tx_csel_enable, 4, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.tx_enable, 4, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //-------------------------------------------------------------//

  epll_reg.push_back(spi1_0);
  epll_reg.push_back(spi1_1);
  epll_reg.push_back(spi1_2);
  epll_reg.push_back(spi1_3);

  return;

}
// ------------------------------------------------------------------------ //
void Config_RocASIC::fillRocASIC_Analog_Int_epll(std::vector<QString> &Int_epll_reg)
{
  stringstream sx;
  if(dbg()) msg()("Loading Internal ePll registers","Config_RocASIC::fillGlobalRegisters");
  int pos = 0;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";


  ROC_ASIC_analog_epll Analog_ePll_obj;
  Analog_ePll_obj = config().ROC_ASIC_Ana_Int_Settings();
  ROC_ASIC_Setting Analog_ePll_global;
  Analog_ePll_global = config().ROC_ASIC_Settings();

  //-----------------------------------//
  //          first 32 bits              
  //-----------------------------------//
  QString spi1_0 = bit32_empty;
  pos = 0;

  QString str;

  str = QString("%1").arg( Analog_ePll_obj.ePll_Ref_Freq,   2, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePll_cap,        2, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePllLockEn,      1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.bypassPll,       1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePllReset,       1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePllInstantLock, 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePll_lcp, 4, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_obj.ePll_res, 4, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePllEnablePhase, 8, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //--------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_0, 7, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_0 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//                                                   
  //                         2nd 32 bits
  //---------------------------------------------------------------//

  QString spi1_1 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_1, 7, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_1 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//                                                   

  str = QString("%1").arg( Analog_ePll_obj.ePllPhase40MHz_2, 7, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( ((Analog_ePll_obj.ePllPhase160MHz_2 & 0b10000) >> 4), 1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//                                                   


  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_0 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_1 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( (Analog_ePll_obj.ePllPhase160MHz_2 & 0b1111), 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.tp_phase_global,   3, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.tp_bypass_global,   1, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//
  //                        3rd 32 bits
  //---------------------------------------------------------------//

  QString spi1_2 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( Analog_ePll_global.testOutMux,  2, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.testOutEn,   1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.LockOutInv,   1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.TDS_BCR_INV,  4, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  str = QString("%1").arg( Analog_ePll_global.VMM_BCR_INV,  8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.VMM_ENA_INV,  8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg( Analog_ePll_global.VMM_L0_INV,  8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//
  //                         4th 32 bits
  //---------------------------------------------------------------//

  QString spi1_3 = bit32_empty;
  pos = 0;

  str = QString("%1").arg( Analog_ePll_global.VMM_TP_INV,  8, 2, QChar('0'));
  spi1_3.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------------------------------------//

  Int_epll_reg.push_back(spi1_0);
  Int_epll_reg.push_back(spi1_1);
  Int_epll_reg.push_back(spi1_2);
  Int_epll_reg.push_back(spi1_3);

}


