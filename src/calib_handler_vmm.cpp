#include  "calib_handler_vmm.h"

using namespace std;

void CalibHandlerVMM::CalibHandlerVMM() 
{
  m_calib_module = new Calib_VMM();
}

void CalibHandlerVMM::set_VMM_Calib_File(std::string str_file) {
  m_calib_module->set_VMM_Calib_File( str_file );
  m_calib_module->init_VMM_Calib_File();
}

void CalibHanderVMM
