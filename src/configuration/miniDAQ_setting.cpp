#include "miniDAQ_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
MiniDAQ_Setting::MiniDAQ_Setting() :
  elink_locked_mask(0),
  TTC_locked_mask(0),
  configured_board_mask(0),
  ok(false)
{
  elink_matrix.resize(0);
  board_UniqueIDs.resize(0);

  std::vector<bool> empty;
  empty.resize(0);
  for ( int i=0; i<32; i++ ) {
    empty.push_back(false);
  }
  for ( int i=0; i<8; i++ ) {
    elink_matrix.push_back(empty);
  }

  for ( int i=0; i<8; i++ ) {
    board_UniqueIDs.push_back( -1 );
  }
}
void MiniDAQ_Setting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Global Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << GlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    cout << ss.str() << endl;
    */

}
