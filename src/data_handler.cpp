
#include "data_handler.h"

#include <QString>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>
#include <QByteArray>
#include <QBitArray>

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#include <boost/filesystem.hpp>
#include "TROOT.h"
#include "TFile.h"

//#include "map_handler.h"
//#include "OnlineMonTool.h"
#include "message_handler.h"

enum reply_type { echo, receiving_fifo_full, target_module_timeout,
                  SCA_reply, SCA_no_error, SCA_Rx_timeout, SCA_error, SCA_I2C_error,
                  data_fifo_full, length_fifo_full, no_reply, event_header, error_undefined };

namespace ip = boost::asio::ip;


DataHandler::DataHandler(QObject* parent) :
  QObject(parent)
{
  m_outDir = "";
  m_dbg    = false;
  m_server = NULL;
}

void DataHandler::LoadMessageHandler(MessageHandler& msg)
{
  m_msg = &msg;
  m_server->LoadMessageHandler(*m_msg);
}

void DataHandler::setDebug(bool dbg)
{
  m_dbg = dbg;
  m_server->setDebug(dbg);
}

void DataHandler::initialize() {

  if ( m_server) {
    //    m_server->stop_listening();
    //    m_server->stop_server();
    delete m_server;
  }

  m_server = new DaqServer();
  m_server->initialize();

  m_server->setEvtCount(0);

}

void DataHandler::setRunStartTime( std::string starttime ) {
  m_server->setRunStartTime(starttime);
}


void DataHandler::setRunEndTime( std::string starttime ) {
  m_server->setRunEndTime(starttime);
}


void DataHandler::setEvtCount(int count) {
  m_server->setEvtCount(count);
}

void DataHandler::setFilenameRaw(std::string filename) 
{
  m_server->setFilenameRaw(filename);
}

void DataHandler::setFilenameMeta(std::string filename)
{
  m_server->setFilenameMeta(filename);
}


void DataHandler::setFilenameDecoded(std::string filename) 
{
  m_server->setFilenameDecoded(filename);
}

void DataHandler::setOutputRaw(bool setOut) 
{
  m_server->setOutputRaw(setOut);
}
void DataHandler::setOutputDecoded(bool setOut) 
{
  m_server->setOutputDecoded(setOut);
}


void DataHandler::gather_data()
{

  stringstream m_sx;

  if ( m_outDir == "" ) {
    m_sx.str("");
    m_sx << "run_" << m_run_number << "_raw";
    setFilenameRaw( m_sx.str() );
    m_sx.str("");
    m_sx << "run_" << m_run_number << "_decoded.root";
    setFilenameDecoded( m_sx.str() );
  }
  else {
    m_sx.str("");
    m_sx << m_outDir << "/run_" << m_run_number << "_raw";
    setFilenameRaw( m_sx.str() );
    m_sx.str("");
    m_sx << m_outDir << "/run_" << m_run_number << "_decoded.root";
    setFilenameDecoded( m_sx.str() );
    m_sx.str("");
    m_sx << m_outDir << "/run_" << m_run_number << "_meta.txt";
    setFilenameMeta( m_sx.str() );
  }

  bool init = m_server->initializeRun();
  if ( !init ) return;

  m_server->listen();
}
  
void DataHandler::endRun()
{

  m_server->stop_listening();
  //m_server->flush();                                                                                                                                         
  m_server->stop_server();
  
  while(true) {
    if ( m_server->is_stopped() ) break;
    msg()("waiting for DAQ server to stop... ", "DataHandler::endRun");
  }
  //  *m_continue_gathering = false;

  m_server->write_output();

}

std::vector<bool> DataHandler::current_locked_elinks() { return m_server->current_locked_elinks(); }
uint32_t          DataHandler::current_EVID()          { return m_server->current_EVID(); }
uint32_t          DataHandler::current_TTC_status()    { return m_server->current_TTC_status(); }
void              DataHandler::set_run_maxevents( int run_max_evt ) { m_server->setMaxRunEvents(run_max_evt); }
