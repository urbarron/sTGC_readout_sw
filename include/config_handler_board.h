#ifndef CONFIG_HANDLER_Board_H
#define CONFIG_HANDLER_Board_H

/////////////////////////////////////////
//
// config_handler
//
// containers for global configuration
// (global, VMM channels, IP/socket information)
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

// vmm
#include "message_handler.h"

#include "comm_info.h"
#include "board_setting.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerBoard : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerBoard(QObject *parent = 0);
        virtual ~ConfigHandlerBoard(){};

        ConfigHandlerBoard& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

	bool LoadBoardConfig_from_File(const QString &filename);
	void WriteBoardConfig_to_File(QString filename);

        CommInfo LoadCommInfo(const boost::property_tree::ptree& p);
        void LoadCommInfo(const CommInfo& info);

	BoardSetting LoadBoardSettings(const boost::property_tree::ptree& p);

        // methods for GUI interaction
	void LoadBoardConfiguration( BoardSetting & board );

        // retrieve the objects
        CommInfo& commSettings()                { return m_commSettings; }
	BoardSetting& BoardSettings()              { return m_board; }

	void SetBoardID( int boardID );
	void SetBoardType( int boardType );
	
    private :
        bool m_dbg;

        CommInfo                m_commSettings;
	BoardSetting            m_board;

        MessageHandler* m_msg;

}; // class ConfigHandlerBoard



#endif
