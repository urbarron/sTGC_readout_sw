#ifndef ROC_ASIC_analog_epll_H
#define ROC_ASIC_analog_epll_H

#include <QStringList>
#include <QList>

class ROC_ASIC_analog_epll {

    public :
        ROC_ASIC_analog_epll();
        virtual ~ROC_ASIC_analog_epll(){};

	//-----------------------------------------------------//
	//                Analog Configurations
	//-----------------------------------------------------//

        uint32_t ePllPhase40MHz_0;
	uint32_t ePllPhase40MHz_1;
	uint32_t ePllPhase40MHz_2;
	uint32_t ePllPhase40MHz_3;

	uint32_t ePllPhase160MHz_0;
        uint32_t ePllPhase160MHz_1;
        uint32_t ePllPhase160MHz_2;
        uint32_t ePllPhase160MHz_3;

	bool ePllInstantLock;
	bool ePllReset;
	bool bypassPll;
	bool ePllLockEn;
	
	uint32_t ePll_Ref_Freq;
	uint32_t ePllEnablePhase;
	uint32_t ePll_lcp;
	uint32_t ePll_cap;
	uint32_t ePll_res;
	
	uint32_t tp_phase_0;
	uint32_t tp_phase_1;
	uint32_t tp_phase_2;
	uint32_t tp_phase_3;
	uint32_t tp_bypass_mask;

	uint32_t ctrl_phase_0;
	uint32_t ctrl_phase_1;
	uint32_t ctrl_phase_2;
	uint32_t ctrl_phase_3;
	uint32_t ctrl_bypass_mask;

	uint32_t ctrl_delay_0;
	uint32_t ctrl_delay_1;
        uint32_t ctrl_delay_2;
        uint32_t ctrl_delay_3;
	uint32_t ctrl_delay_mask;

	uint32_t tx_enable;
	uint32_t tx_csel_enable;

	//----------------------------------------//

        void validate();
        void print();
        bool ok; // loading went ok

};

#endif
