#ifndef CONFIG_MODULE_ROC_ASIC_H
#define CONFIG_MODULE_ROC_ASIC_H

/////////////////////////////////////////
//
// configuration_module
//
// Tool for building and sending the
// configuration packets to the front-end/
// VMMs
//
//  - sends the global SPI and configuration
//    of all the VMM channels
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include "config_module_base.h"
#include "config_handler_roc_asic.h"
#include "socket_handler.h"
#include "message_handler.h"

// Qt
#include <QString>


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_ROC_ASIC
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////

class Config_RocASIC : public Config_Base
{

    public :
        explicit Config_RocASIC(Config_Base *parent = 0);
        virtual ~Config_RocASIC(){};

        Config_RocASIC& LoadConfig(ConfigHandlerRocASIC& config);

        bool SendRocASIC_DigitalConfig(int send_to_port, const QHostAddress & target_ip);
	bool SendRocASIC_AnalogConfig( int send_to_port, const QHostAddress & target_ip);

	void fillRocASIC_DigitalRegisters( std::vector<QString>& globalRegisters);
	void fillRocASIC_Analog_epll(      std::vector<QString>& epll_reg, int i_ePll);
	void fillRocASIC_Analog_Int_epll(  std::vector<QString>& Int_epll_reg);

        ConfigHandlerRocASIC& config() { return *m_configHandler; }

 private :

	ConfigHandlerRocASIC *m_configHandler;

	const static int VMM0_ePll= 0;
	const static int VMM1_ePll= 1;
        const static int TDS_ePll= 2;
        const static int Int_ePll= 3;
	
 signals :

	public slots :
       
}; // class Config_RocASIC

#endif
