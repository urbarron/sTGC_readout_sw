#ifndef VMM_HIT_H
#define VMM_HIT_H

//////////////////////////////////////////
//
// vmm_hit
//
// Structure to hold the VMM hit info
//
// daniel.joseph.antrim@cern.ch
// February 2017
//
//////////////////////////////////////////

// std/stl
#include <stdint.h>

class VMMHit
{

    public :
        VMMHit();
        virtual ~VMMHit(){};

        /////////////////////////////////
        // good ol' getters
        /////////////////////////////////
        uint32_t get_flag() { return m_flag; }
        uint32_t get_pdo() { return m_pdo; }
        uint32_t get_tdo() { return m_tdo; }
        uint32_t get_graycode_bcid() { return m_graycode_bcid; }
        uint32_t get_decoded_graycode_bcid() { return m_decoded_graycode_bcid; }
        uint32_t get_pass_threshold() { return m_pass_threshold; }
        int32_t  get_vmm_channel() { return m_vmm_channel; }
        int32_t  get_mapped_channel() { return m_mapped_channel; }
        int32_t  get_feb_channel() { return m_feb_channel; }
        int32_t  get_board_id() { return m_board_id; }

        /////////////////////////////////
        // sure can set 'em
        /////////////////////////////////
        void set_flag(uint32_t flag) { m_flag = flag; }
        void set_pdo(uint32_t pdo) { m_pdo = pdo; }
        void set_tdo(uint32_t tdo) { m_tdo = tdo; }
        void set_graycode_bcid(uint32_t graycode) { m_graycode_bcid = graycode; }
        void set_decoded_graycode_bcid(uint32_t decoded) { m_decoded_graycode_bcid = decoded; }
        void set_pass_threshold(uint32_t threshold) { m_pass_threshold = threshold; }
        void set_vmm_channel(int32_t channel) { m_vmm_channel = channel; }
        void set_mapped_channel(int32_t channel) { m_mapped_channel = channel; }
        void set_feb_channel(int32_t channel) { m_feb_channel = channel; }
        void set_board_id(int32_t boardid) { m_board_id = boardid; } 

        void print();
        void clear();

    private :

        /////////////////////////////////
        // VMM hit info
        /////////////////////////////////
        uint32_t m_flag;
        uint32_t m_pdo;
        uint32_t m_tdo;
        uint32_t m_graycode_bcid;
        uint32_t m_decoded_graycode_bcid;
        uint32_t m_pass_threshold;
        int32_t  m_vmm_channel;
        int32_t  m_mapped_channel;
        int32_t  m_feb_channel;
        int32_t  m_board_id;
        
        

}; // class


#endif
