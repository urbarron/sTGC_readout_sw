#ifndef CONFIG_HANDLER_ROC_FPGA_H
#define CONFIG_HANDLER_ROC_FPGA_H

/////////////////////////////////////////
//
// config_handler
//
// containers for global configuration
// (global, VMM channels, IP/socket information)
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

#include "message_handler.h"
#include "roc_fpga_setting.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerRocFPGA : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerRocFPGA(QObject *parent = 0);
        virtual ~ConfigHandlerRocFPGA(){};

        ConfigHandlerRocFPGA& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

	void SetBoardID( int boardID );
	void SetBoardType( int boardType );

	bool Load_ROC_FPGAConfig_from_File(const QString &filename);
        void Write_ROC_FPGAConfig_to_File(QString filename);

	ROC_FPGASetting Load_ROC_FPGASettings(const boost::property_tree::ptree& p);

        // methods for GUI interaction
	void Load_ROC_FPGAConfiguration(ROC_FPGASetting & global);
	void Set_ROC_Registered_to_Default(int boardID, int boardType);

	ROC_FPGASetting& ROC_FPGASettings() { return m_roc_fpga_settings; }

	QString ROC_FPGAMask();

    private :
        bool m_dbg;

	ROC_FPGASetting         m_roc_fpga_settings;

        MessageHandler* m_msg;

}; // class ConfigHandler



#endif
