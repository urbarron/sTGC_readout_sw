#ifndef DAQ_BUFFER_H
#define DAQ_BUFFER_H

///////////////////////////////////////////
//
// daq_buffer
//
// Queue that handles the incoming data
// packets from the front-ends
//
// Wrapper around the lock-free, single-producer,
// single-consumer queue developed by Cameron Desrochers,
// description at [1] and source at [2]
//
// [1] http://moodycamel.com/blog/2013/a-fast-lock-free-queue-for-c++
// [2] https://github.com/cameron314/readerwriterqueue 
//
// daniel.joseph.antrim@cern.ch
// University of California, Irvine
// October 2016
//
///////////////////////////////////////////

// moodycamel
#include "readerwriterqueue.h"
#include "atomicops.h"

// boost
#include <boost/shared_ptr.hpp>

//nsw
#include "data_array_types.h" // IPDataArray

class DaqBuffer
{
    public :
        explicit DaqBuffer();
        virtual ~DaqBuffer(){};


        void initialize(size_t capacity = 15);

        bool has_data();

        //bool dequeue(int& element);
        bool dequeue(IPDataArray& element);

        //bool add_to_queue(int element);
        bool add_to_queue(IPDataArray& element);

        

    private :

        //boost::shared_ptr<moodycamel::ReaderWriterQueue<int> > m_queue;
        boost::shared_ptr<moodycamel::ReaderWriterQueue<IPDataArray> > m_queue;
        size_t m_capacity;

};  // class



#endif
