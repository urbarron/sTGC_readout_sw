#ifndef DAQ_SERVER_H
#define DAQ_SERVER_H

#include <stdio.h>

#include <QObject>

#include <sstream>
#include <fstream>

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>

#include <string.h>

//nsw 

//#include "event_builder.h"
#include "data_array_types.h" // RawDataArray 
#include "vmm_decoder.h"
                        
//#include "calibration_module.h"
//#include "daq_monitor.h"
//class MapHandler;
//class OnlineMonTool;
class MessageHandler;
//class CalibrationState;
class DaqBuffer;

class DaqServer : public QObject
{
    Q_OBJECT

 public :
    explicit DaqServer(QObject *parent=0);
    virtual ~DaqServer();

    void LoadMessageHandler(MessageHandler& msg);
    MessageHandler& msg() { return *m_msg; }

    void setDebug(bool dbg);

    void initialize();
    bool initializeRun();
    
    void listen();

    void handle_data();

    void setRunStartTime(std::string starttime);
    void setRunEndTime(std::string endtime);

    //    void gather_data(const boost::system::error_code error, std::size_t size_);

    void gather_data(const boost::system::error_code error, std::size_t size_);
    void setEvtCount( int count ) { evt_count = count; }
    
    std::vector<bool> current_locked_elinks() { return m_current_locked_elink; }
    uint32_t          current_EVID()          { return m_current_EVID; }
    uint32_t          current_TTC_status()    { return m_current_TTC_status; }

    void setMaxRunEvents( int m_run_max_evt ) { m_run_max_events = m_run_max_evt; }
    void setRunNumber   ( int number )        { m_run_number = number; }

    //DaqBuffer                                                                                                                                         
    void read_data();

    bool is_stopped();
    void stop_listening();
    void stop_server();

    //    void endRun();
    void write_output();

    void InitRawTTree();
    void InitDecodedTTree();

    void setOutputRaw(bool setOut) {    output_raw = setOut;}
    void setOutputDecoded(bool setOut) {output_decoded = setOut;}

    void setFilenameRaw(std::string filename)     { filename_raw = filename; }
    void setFilenameMeta(std::string filename)    { filename_meta = filename; }
    void setFilenameDecoded(std::string filename) { filename_decoded = filename; }

    void outputDatagram(std::vector<uint32_t> datagram);

    std::vector<uint32_t> current_TTC_reg_address() { return m_decode->current_TTC_reg_address(); }
    std::vector<uint32_t> current_TTC_reg_value()   { return m_decode->current_TTC_reg_value(); }

 private :

    //    boost::shared_ptr<std::ofstream> m_ofile;
    std::ofstream m_tex_ofile;
    FILE *m_ofile;
    std::ofstream m_ofile_meta;

    bool output_raw;
    bool output_decoded;

    bool write_tex;
    bool write_bin;

    std::string filename_raw;
    std::string filename_meta;
    std::string filename_decoded;

    TFile *outFile;
    TTree *out_decoded_tree;

    int m_run_number;

    boost::shared_ptr<VMMDecoder> m_decode;

    std::vector<uint32_t> m_tdo;
    std::vector<uint32_t> m_pdo;
    std::vector<uint32_t> m_chan;
    std::vector<uint32_t> m_vmm_id;
    std::vector<uint32_t> m_bcid_rel;
    std::vector<uint32_t> m_flag;

    std::vector<uint32_t> m_board_id;
    std::vector<uint32_t> m_bcid;
    std::vector<uint32_t> m_l1_id;

    std::vector<bool> m_current_locked_elink;
    uint32_t          m_current_EVID;
    uint32_t          m_current_TTC_status;

    int m_run_max_events;

    boost::shared_ptr<bool> m_continue_gathering; // common to all threads

    TString m_ip;
    TString m_data;

    MessageHandler* m_msg;
    bool m_dbg;
    std::stringstream m_sx;

    bool dbg() { return m_dbg; }

    RawDataArray m_data_buffer;
    boost::asio::ip::udp::endpoint m_remote_endpoint;

    int evt_count;

    int m_thread_count;
    boost::thread_group m_thread_group;

    boost::shared_ptr<DaqBuffer> m_buffer;

    boost::shared_ptr<boost::asio::ip::udp::socket> m_socket;
    boost::shared_ptr<boost::asio::io_service> m_io_service;
    boost::shared_ptr<boost::asio::io_service::work> m_idle_work;
};

#endif
